import Login from "../pages/Login"
import Books from "../pages/Books"
import BookDetails from "../pages/BookDetails"
import NotFound from '../components/NotFound'
import Admin from "../pages/Admin"
const routes = [
    {
        path : '/' , 
        component : Login
    } , 
    {
        path : '/admin' , 
        component : Admin
    } , 
    {
        path : '/book' , 
        component : Books
    } ,
    {
        path : '/detail' , 
        component : BookDetails
    } , 
    {
        path : '*' , 
        component : NotFound
    } 
]
export default routes
