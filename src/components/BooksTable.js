import React , { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import booksContext from '../context/Books'
import Tags from './Tags';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import bookDetailContext from '../context/BookDetail';
const useStyles = makeStyles({
  table: {
    minWidth: 650
  },
  tableContainer : {
      height:"600px"
  },
  button : {
    fontSize : "12px"
  }
});
var dateFormat = require("dateformat");
const BooksTable = () => {
  const classes = useStyles();
    const tableHead = ["Title" , "CreatedAt" , "Tags" , "Detail"]
    const {filteredBooks} = useContext(booksContext)
    const {setBookDetail} = useContext(bookDetailContext)
    let history = useHistory();
    const changeRoute = ( item ) => {
      setBookDetail(item)
      history.push("/detail"); 
    }
  return (
    <TableContainer component={Paper} dir="rtl" className={classes.tableContainer}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
              { tableHead.map((item , index) => <TableCell align="center" key={index}>{item}</TableCell>) }
          </TableRow>
        </TableHead>
        <TableBody>
          {filteredBooks.map((item , index ) => (
            <TableRow key={index}>
              <TableCell align="center"><p>{item.title}</p></TableCell>
              <TableCell align="center">{item.createdAt ? dateFormat( item.createdAt , "fullDate" ): "----"}</TableCell>
              <TableCell align="center"><Tags tags={item.tags}/></TableCell>
              <TableCell align="center"><Button variant="contained" color="primary" size="small" className={classes.button} onClick={ () => changeRoute( item )}> Detail </Button></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
export default BooksTable