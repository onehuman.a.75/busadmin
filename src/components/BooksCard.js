import React , { useContext }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import booksContext from '../context/Books';
import Card from './Card';
const useStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: theme.spacing(3),
  },
  Card: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing(1),
  },
}));

const BooksCard = () =>  {
  const classes = useStyles();
  const { filteredBooks } = useContext(booksContext)
  return (
    <div>
      <Grid container spacing={3}>
          {filteredBooks.map( ( item , index ) => 
          <Grid item xs={3} key={index}>
            <Card className={classes.Card} item={item} />
          </Grid>
        )}
        
      </Grid>
    </div>
  );
}
export default BooksCard
