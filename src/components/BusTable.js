import React  , { useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { getBusInformations , deleteItem } from '../services'
import { connect } from 'react-redux'
import { setBusInformations } from '../Redux/mainActions'
import DeleteIcon from '@material-ui/icons/Delete';
import Toast from '../components/Toast'
import EditBusModal from '../components/EditBusModal'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#3f51b5" ,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles({
  table: {
    minWidth: 700 ,
    direction : "rtl"   },
    edit : {
      cursor : "pointer",
      color : "green"
    } , 
    delete : {
      cursor : "pointer",
      color : "red"
    }
});

const CustomizedTables = ( props ) => {
  const classes = useStyles();
  const getInformations = async () => { 
    const {data} = await getBusInformations()
    props.setBusInformations( data.data )
   }
  useEffect( () => {
      getInformations()
  } , [] )
  const deleteBusInformation = async ( id ) => {
    const data = await deleteItem( id )
    getInformations()
    console.log( data )
  if( data && ( data.status === 200 || data.status === 201 || data.status === 202 ) ){
    Toast( "ایتم با موفقیت حذف شد ", "white" , "green" )
  }
  }
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center"><p> ویرایش </p></StyledTableCell>
            <StyledTableCell align="center"><p> حذف </p></StyledTableCell>
            <StyledTableCell align="center"><p> کد خط اتوبوس </p></StyledTableCell>
            <StyledTableCell align="center"><p> هزینه کارتی </p></StyledTableCell>
            <StyledTableCell align="center"><p> هزینه نقدی </p></StyledTableCell>
            <StyledTableCell align="center"><p> خط اتوبوس </p></StyledTableCell>
            <StyledTableCell align="center"><p> ساعت حرکت </p></StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.busInformations.map((row) => (
            <StyledTableRow key={row.id}>
              <StyledTableCell align="center"><EditBusModal id = { row.id }/></StyledTableCell>
              <StyledTableCell align="center"><DeleteIcon className = { classes.delete } onClick={ () => deleteBusInformation( row.id )}/></StyledTableCell>
              <StyledTableCell component="th" scope="row" align="center">
                <p>{row.code}</p>
              </StyledTableCell>
              <StyledTableCell align="center"><p>{row.card_price + " تومان "}</p></StyledTableCell>
              <StyledTableCell align="center"><p>{row.cash_price + " تومان "}</p></StyledTableCell>
              <StyledTableCell align="center"><p>{row.station_name}</p></StyledTableCell>
              <StyledTableCell align="center"><p>{row.start_time}</p></StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
const mapStateToProps = state => {
	return {
		busInformations: state.busInformations
	}
}
const mapDispatchToProps = dispatch => {
	return {
		setBusInformations: ( value ) => dispatch(setBusInformations( value )),
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomizedTables) 
