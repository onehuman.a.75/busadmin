import React , { useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaterialCard from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Tags from './Tags';
import { useHistory } from "react-router-dom";
import bookDetailContext from '../context/BookDetail';
const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    height: 350
  },
  media: {
    height: 200,
  },
  title : {
    fontFamily : "BNazanin" , 
    fontWeight : "bold" , 
    fontSize : "large" , 
    direction : "rtl"
  }
});

const Card = ( { item } ) =>  {
  const classes = useStyles();
  let history = useHistory();
  const {setBookDetail} = useContext(bookDetailContext)
    const changeRoute = ( item ) => {
      setBookDetail(item)
      history.push("/detail"); 
    }
  return (
    <MaterialCard className={classes.root} onClick={ () => changeRoute( item )}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={item.picture}
          title={item.title}
        />
        <CardContent>
          <Typography gutterBottom className={classes.title}>
            {item.title}
          </Typography>
        </CardContent>
      </CardActionArea>
      <Tags tags={item.tags}/>

    </MaterialCard>
  );
}
export default Card
