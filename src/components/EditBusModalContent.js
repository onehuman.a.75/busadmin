import React , { useState ,useEffect} from 'react';
import { createTheme , ThemeProvider , makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import { setCode , setStationName , setImage , setCardPrice , setCashPrice ,setStartTime , setBusInformations } from '../Redux/mainActions'
import { getBusInformation , updateBusInformation , getBusInformations } from '../services'
import Toast from '../components/Toast'

const theme = createTheme({
    direction: 'rtl',
  });
  const useStyles = makeStyles((theme) => ({
    row: {
      display: 'flex',
      flexDirection: 'row',
    } , 
    buttonRow: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center' , 

    } ,
    head : {
      display : 'flex',
      justifyContent: 'center'
    }
  }));
 const AddBusModalContent = ( props ) =>  {
   const { row  , buttonRow , head } = useStyles() ; 
   const [ uploadedImage , setUploadedImage ] = useState( null ) ;
   const [ selectValue , setSelectValue ] = useState( "/" ) ;
   const [ submitText , setSubmitText ] = useState("ویرایش")
   const [ changedImage , setChangedImage ] = useState( false )
   const fileUploadInputChange = ( event ) => {
      setChangedImage(true)
      setUploadedImage( URL.createObjectURL( event.target.files[0] ) )
      getBase64( event.target.files[0]  , ( result ) => {
        props.setImage( result )
   })
   
}
const getBase64FromUrl = async ( url , cb ) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob); 
      reader.onloadend = () => {
        const base64data = reader.result;  
        cb(base64data)
        resolve(base64data);
      }
    });
  }
const getBase64 = ( file , cb ) => {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    cb( reader.result )
  };
  reader.onerror = function (error) {
      console.log('Error: ', error);
  };
}
 const selectChange = ( select ) => {
    setSelectValue( select.value );
    var selectedOption = select.options[select.selectedIndex];
    var name = selectedOption.getAttribute('name');
    props.setStationName( name );
  }
  const getInformations = async () => { 
    const {data} = await getBusInformations()
    props.setBusInformations( data.data )
   }
  const update = async ( id ) => {
    setSubmitText(" لطفا صبر کنید ")
    const { code , start_time , station_name , image , card_price , cash_price } = props
    if( !changedImage ){
        getBase64FromUrl( props.image , async ( result ) => {
            const data = await updateBusInformation( id , {
                code , 
                start_time , 
                station_name , 
                result , 
                card_price , 
                cash_price
            })
            if( data && ( data.status === 200 || data.status === 201 || data.status === 202 ) ){
              Toast( "اطلاعات با موفقیت ویرایش شد", "white" , "green" )
              props.close()
            }
            getInformations()
            console.log( data )
       })
    }else{
        const data = await updateBusInformation( id , {
            code , 
            start_time , 
            station_name , 
            image , 
            card_price , 
            cash_price
        })
        if( data && ( data.status === 200 || data.status === 201 || data.status === 202 ) ){
          Toast( "اطلاعات با موفقیت ویرایش شد", "white" , "green" )
          props.close()
        }
        getInformations()
        console.log( data )
    }
  }
  const getInformation = async ( id ) => {
    const {data} = await getBusInformation( id );
        props.setCode( data.data.code ) 
		setUploadedImage( data.data.image ) 
        props.setImage( data.data.image )
        props.setCardPrice( data.data.card_price )
        props.setCashPrice( data.data.cash_price )
        props.setStartTime( data.data.start_time )
    console.log(data)
  }
  useEffect( () => {
    getInformation( props.id )
} , [] )
  return (   
    <ThemeProvider theme={theme}>
      <div dir="rtl">
        <div className={row}>
        <div className="galaxy-input">
                        <input value={ props.code } type="text" autoComplete="off" required="required" onChange={ ( e ) => props.setCode( e.target.value )} />
                        <label>کد اتوبوس</label>
                        <div className="bar"></div>
                        <span className="mdi mdi-email-outline"></span>
        </div>
        <div className="galaxy-input">
                        <input type="text" value={props.card_price} autoComplete="off" required="required" onChange={ ( e ) => props.setCardPrice( e.target.value )} />
                        <label>هزینه کارتی</label>
                        <div className="bar"></div>
                        <span className="mdi mdi-email-outline"></span>
        </div>
        <div className="galaxy-input">
                        <input type="text" value={props.cash_price} autoComplete="off" required="required" onChange={ ( e ) => props.setCashPrice( e.target.value )}  />
                        <label>هزینه نقدی</label>
                        <div className="bar"></div>
                        <span className="mdi mdi-email-outline"></span>
        </div>
        <div className="galaxy-input">
                        <input type="text" value={props.start_time} autoComplete="off" required="required" onChange={ ( e ) => props.setStartTime( e.target.value )} />
                        <label>ساعت حرکت</label>
                        <div className="bar"></div>
                        <span className="mdi mdi-email-outline"></span>
        </div>
        <div className="galaxy-select">
							<select onChange={ e => selectChange( e.target )} value={ selectValue } >
								<option name="" value="/"></option>
								<option name="خط اطراف حرم" value="1">خط اطراف حرم</option>
							</select>
							<label>خط اتوبوس</label>
							<div className="bar"></div>
							<span className="mdi mdi-car-hatchback"></span>
				</div>
        </div>
        <br />
        <div className={head}>
          <div className="text-center font_10">اپلود عکس تبلیغات</div>
        </div>	
						<label className="uploader">
							<input type="file" accept="image/*"  onChange = { ( e ) =>  fileUploadInputChange( e )}/>
						</label>
                        <img src={uploadedImage} alt={""} style={{marginTop: "10px"}}  /> 
				
        <br />
        <br/>
        <div className={buttonRow}> 
          <button className="galaxy-button" style={{backgroundColor: "green"}} onClick={() => update( props.id )}> { submitText } </button>
          <button className="galaxy-button" style={{backgroundColor: "red"}} onClick={ () => props.close()}> انصراف </button>
        </div>
      </div>
    </ThemeProvider>
  );
}
const mapStateToProps = state => {
	return {
        allState : state , 
		code: state.code,
		start_time: state.start_time,
		station_name: state.station_name,
        image: state.image,
        card_price: state.card_price,
        cash_price: state.cash_price
	}
}
const mapDispatchToProps = dispatch => {
	return {
		setCode: ( value ) => dispatch(setCode( value )),
		setStationName: ( value ) => dispatch(setStationName( value )),
		setImage: ( value ) => dispatch(setImage( value )),
        setCardPrice: ( value ) => dispatch(setCardPrice( value )),
        setCashPrice: ( value ) => dispatch(setCashPrice( value )),
        setStartTime: ( value ) => dispatch(setStartTime( value )) , 
        setBusInformations: ( value ) => dispatch(setBusInformations( value ))
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(AddBusModalContent) 