import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
  button : {
      fontSize : "10px"
  }
}));

const Tags = ( { tags } ) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
        { tags ?  tags.map( ( item , index ) => <Button variant="outlined" color="primary" key={index} className={classes.button}> { item } </Button>)  : <div> </div>}
    </div>
  );
}
export default Tags