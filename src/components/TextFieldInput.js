import React from 'react';
import { withStyles , makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

  const styles = {
    'input': {
      '&::placeholder': {
        fontFamily : "Yekan"
      }
    }
  };
  const useStyles = makeStyles((theme) => ({
    input: {
      fontFamily : "Yekan" , 
      margin : "5px"
    }
  }));
 const TextFieldInput = ( { name , classes } ) =>  {
   const { input } = useStyles()
  return (   
      <div dir="rtl">
        <TextField placeholder={ name } variant="outlined" className={input}
        InputProps={{ classes: {input: classes[ 'input' ] } }}
        />
      </div>
  );
}
export default withStyles(styles)(TextFieldInput);