import { Redirect } from 'react-router-dom';
import Header from '../components/Header';
import BusTable from '../components/BusTable';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    busSAtationTable : {
        margin : "50px"
    } , 
  }));
const Admin = () => { 
    const { busSAtationTable } = useStyles()
    // if( !localStorage.getItem("login") || localStorage.getItem("login") === false ){
    //     return <Redirect to="/"/>
    // } else {
        return (
            <div>
                <Header />  
                <div className={ busSAtationTable }>
                    <BusTable />
                </div>
            </div>
        )
    // }
}
export default Admin