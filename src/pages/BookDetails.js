import React , { useContext }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tags from '../components/Tags';
import bookDetailContext from '../context/BookDetail';
import { Redirect } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  root: {
    width : "40%",
    height: "30%", 
    backgroundColor : "#ecf2f9"
  },
  container : {
    marginTop : theme.spacing(10) ,
    display : "flex" ,
    justifyContent : "center" ,
    alignItems : "center" , 
  } , 
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  description : {
      fontFamily : "BNazanin" , 
      fontWeight : "bold"
  }
}));
var dateFormat = require("dateformat");
const BookDetails = () =>  {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { bookDetail } = useContext(bookDetailContext)
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  if( !localStorage.getItem("login") || localStorage.getItem("login") === false ){
    return <Redirect to="/"/>
} else {
  return (
    <div className={classes.container}>
        <Card className={classes.root}>
          <CardHeader
            dir="rtl"
            title={<h5>{bookDetail.title}</h5>}
            subheader={ bookDetail.createdAt ? dateFormat( bookDetail.createdAt , "fullDate" ) : "----" }
          />
          <CardMedia
            className={classes.media}
            image={bookDetail.picture }
          />
          <CardContent>
            <Typography color="textSecondary" className={classes.description}>
              {bookDetail.description}
            </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
                <Tags tags={bookDetail.tags}/>
            </CardContent>
          </Collapse>
        </Card>
    </div>
  );
    }
}
export default BookDetails
